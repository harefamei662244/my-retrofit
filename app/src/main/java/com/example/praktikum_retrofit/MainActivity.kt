package com.example.praktikum_retrofit

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.liveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.praktikum_retrofit.databinding.ActivityMainBinding
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    private lateinit var albumService: AlbumService
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        albumService = RetrofitInstance.getRetrofitInstance().create(AlbumService::class.java)

        initRecyclerView()
        // Get All Albums
//        loadDataAlbumsFromAPI()
        // Get one albums item
//        loadDataUsingPathParameter()
        // Tambah Data album
//        uploadAlbum()
    }

    //Init Recycler View
    private fun initRecyclerView() {
        binding.rvAlbums.layoutManager = LinearLayoutManager(this)
        loadDataAlbumsFromAPI()
    }

    private fun loadDataAlbumsFromAPI() {
        val responData: LiveData<Response<Albums>> = liveData {
            // Get All Albums
            val response = albumService.getAlbums()
            // Get Albums with Query
//            val response = albumService.getAlbumsWithQuery(2)
            emit(response)
        }
        responData.observe(this, Observer {
            binding.rvAlbums.adapter = AlbumAdapter(
                it,
                { selectedAlbum: AlbumsItem -> loadDataUsingPathParameter(selectedAlbum) }
            )
        })
    }

    private fun loadDataUsingPathParameter(albumId: AlbumsItem) {
        val responData: LiveData<Response<AlbumsItem>> = liveData {
            val response = albumService.getAlbum(albumId.id)
            emit(response)
        }
        responData.observe(this, Observer {
            val title = it.body()?.title.toString()
            Toast.makeText(this, title, Toast.LENGTH_LONG).show()
        })
    }

//    private fun uploadAlbum() {
//        val album = AlbumsItem(0, "Lagu Indonesia Pop", 1)
//        val responData: LiveData<Response<AlbumsItem>> = liveData {
//            val response = albumService.postAlbum(album)
//            emit(response)
//        }
//        responData.observe(this, Observer {
//            val albumRecieved = it.body()
//            val result = "Album Title : ${albumRecieved?.title} \n" +
//                    "Album Id : ${albumRecieved?.id} \n" +
//                    "Album User Id : ${albumRecieved?.userId} \n\n\n"
//            tvAlbums.text = result
//        })
//    }
}