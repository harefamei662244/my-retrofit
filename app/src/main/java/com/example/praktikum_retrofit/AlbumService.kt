package com.example.praktikum_retrofit

import retrofit2.Response
import retrofit2.http.*

interface AlbumService {
    @GET("/albums")
    suspend fun getAlbums(): Response<Albums>

    @GET("/albums")
    suspend fun getAlbumsWithQuery(@Query("userId") userId: Int): Response<Albums>

    @GET("/albums/{albumId}")
    suspend fun getAlbum(@Path(value = "albumId") albumId: Int?): Response<AlbumsItem>

    @POST("/albums")
    suspend fun postAlbum(@Body album: AlbumsItem): Response<AlbumsItem>
}