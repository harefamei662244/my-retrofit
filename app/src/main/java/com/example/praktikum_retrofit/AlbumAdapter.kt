package com.example.praktikum_retrofit

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.praktikum_retrofit.databinding.ItemAlbumsBinding
import retrofit2.Response

class AlbumAdapter(
    private val albums: Response<Albums>,
    private val clickListener: (AlbumsItem) -> Unit
) : RecyclerView.Adapter<MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val albumsBinding = ItemAlbumsBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return MyViewHolder(albumsBinding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(albums.body()?.get(position), clickListener)
    }

    override fun getItemCount(): Int {
        return albums.body()!!.size
    }

}

class MyViewHolder(val binding: ItemAlbumsBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(albums: AlbumsItem?, clickListener: (AlbumsItem) -> Unit) {
        binding.apply {
            tvTitleAlbum.text = "Albums Title : " + albums?.title

            tvIdAlbum.text = "Albums Id : " + albums?.id.toString()

            tvUserIdAlbum.text = "Albums UserId : " + albums?.userId.toString()

            evAlbums.setOnClickListener {
                if (albums != null) {
                    clickListener(albums)
                }
            }
        }
    }
}